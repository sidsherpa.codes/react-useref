import React, { useRef, useState } from "react";

const UseRefDivCss = () => {
  const divRef = useRef();
  const colorRef = useRef();
  const fontSizeRef = useRef();
  const textRef = useRef();
  const [changesMade, setChangesMade] = useState(false);

  const changeColor = () => {
    colorRef.current = divRef.current.style.color;
    divRef.current.style.color = "blue";
    setChangesMade(true);
  };

  const changeFontSize = () => {
    fontSizeRef.current = divRef.current.style.fontSize;
    divRef.current.style.fontSize = "50px";
    setChangesMade(true);
  };

  const changeText = () => {
    textRef.current = divRef.current.textContent;
    divRef.current.textContent = "Div after";
    setChangesMade(true);
  };

  const revertColor = () => {
    if (changesMade) {
      divRef.current.style.color = colorRef.current;
      setChangesMade(false);
    }
  };

  const revertFontSize = () => {
    if (changesMade) {
      divRef.current.style.fontSize = fontSizeRef.current;
      setChangesMade(false);
    }
  };

  const revertText = () => {
    if (changesMade) {
      divRef.current.textContent = textRef.current;
      setChangesMade(false);
    }
  };

  return (
    <div>
      <h3>useRef Div Css</h3>
      <div ref={divRef}>Div before</div>

      <button type="button" onClick={changeColor}>
        Change color of div
      </button>
      <button type="button" onClick={changeFontSize}>
        Change font size of div
      </button>
      <button type="button" onClick={changeText}>
        Change text of div
      </button>

      <button type="button" onClick={revertColor}>
        Revert color
      </button>
      <button type="button" onClick={revertFontSize}>
        Revert font size
      </button>
      <button type="button" onClick={revertText}>
        Revert text
      </button>
    </div>
  );
};

export default UseRefDivCss;
