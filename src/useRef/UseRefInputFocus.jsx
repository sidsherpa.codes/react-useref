import React, { useRef } from "react";

const UseRefInputFocus = () => {
  let inputRef1 = useRef(null); // Defining ref
  let inputRef2 = useRef(null);
  let inputRef3 = useRef(null);

  console.log(inputRef1); // returns an object with a current property
  console.log(inputRef1.current); // returns a reference to the dom node

  return (
    <div>
      <h1>React useRef</h1>
      <h3>useRef Input Focus</h3>
      <form>
        <label htmlFor="name1">Name</label>
        <input
          id="name1"
          type="text"
          placeholder="Name"
          ref={inputRef1} // Attach ref
        ></input>
        <br />
        <input
          id="address"
          type="text"
          placeholder="Address"
          ref={inputRef2} // Attach ref
        ></input>
        <br />
        <input
          id="phoneNumber"
          type="text"
          placeholder="Phone Number"
          ref={inputRef3} // Attach ref
        ></input>
        <br />
        <button
          type="button"
          onClick={() => {
            inputRef1.current.focus();
            // inputRef is an object
            // current is a property that references it's DOM node
            // focus is a method
          }}
        >
          Click on Name
        </button>
        <button
          type="button"
          onClick={() => {
            inputRef2.current.focus();
          }}
        >
          Click on Address
        </button>
        <button
          type="button"
          onClick={() => {
            inputRef3.current.focus();
          }}
        >
          Click on Phone number
        </button>
      </form>
    </div>
  );
};

export default UseRefInputFocus;
