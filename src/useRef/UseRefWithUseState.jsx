import React, { useRef, useState } from "react";

const UseRefWithUseState = () => {
  const divRef = useRef();
  const [previousColor, setPreviousColor] = useState(null);
  const [previousFontSize, setPreviousFontSize] = useState(null);
  const [previousText, setPreviousText] = useState("Div before");

  const changeColor = () => {
    setPreviousColor(divRef.current.style.color);
    divRef.current.style.color = "blue";
  };

  const changeFontSize = () => {
    setPreviousFontSize(divRef.current.style.fontSize);
    divRef.current.style.fontSize = "50px";
  };

  const changeText = () => {
    setPreviousText(divRef.current.textContent);
    divRef.current.textContent = "Div after";
  };

  const revertColor = () => {
    divRef.current.style.color = previousColor;
  };

  const revertFontSize = () => {
    divRef.current.style.fontSize = previousFontSize;
  };

  const revertText = () => {
    divRef.current.textContent = previousText;
  };

  return (
    <div>
      <h3>useRef with useState</h3>
      <h4><i>Combining useRef and useState</i></h4>
      <div ref={divRef}>Div before</div>

      <button type="button" onClick={changeColor}>
        Change color of div
      </button>
      <button type="button" onClick={changeFontSize}>
        Change font size of div
      </button>
      <button type="button" onClick={changeText}>
        Change text of div
      </button>

      <button type="button" onClick={revertColor}>
        Revert color
      </button>
      <button type="button" onClick={revertFontSize}>
        Revert font size
      </button>
      <button type="button" onClick={revertText}>
        Revert text
      </button>
    </div>
  );
};

export default UseRefWithUseState;
