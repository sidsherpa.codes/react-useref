import React, { useRef, useState } from "react";

const UseRefVsUseState = () => {
  console.log("Component rendered");
  const [count1, setCount1] = useState(0);

  const countRef = useRef(0); // Create a ref and initialize it with 0

  const incrementCountRef = () => {
    countRef.current++; // Increment the current value of the ref
    countRef.currentElement.textContent = `countRef is ${countRef.current}`; // Update count display
  };

  const decrementCountRef = () => {
    countRef.current--; // Decrement the current value of the ref
    countRef.currentElement.textContent = `countRef is ${countRef.current}`; // Update count display
  };

  const resetCountRef = () => {
    countRef.current = 0; // Reset the current value of the ref
    countRef.currentElement.textContent = `countRef is ${countRef.current}`; // Update count display
  };

  return (
    <div>
      <h1>React useRef</h1>
      <h3>useRef vs useState</h3>
      <div>
        <h4>
          <i>useState</i>
        </h4>
        <p>count1 is {count1}</p>
        <button onClick={() => setCount1(count1 + 1)}>Increment count</button>
        <button onClick={() => setCount1(count1 - 1)}>Decrement count</button>
        <button onClick={() => setCount1(0)}>Reset count</button>
      </div>
      <br />
      <div>
        <h4>
          <i>useRef</i>
        </h4>
        <p ref={(e) => (countRef.currentElement = e)}>
          countRef is {countRef.current}
        </p>
        <button onClick={incrementCountRef}>Increment count</button>
        <button onClick={decrementCountRef}>Decrement count</button>
        <button onClick={resetCountRef}>Reset count</button>
      </div>
    </div>
  );
};

export default UseRefVsUseState;
