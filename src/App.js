import "./App.css";
import UseRefDivCss from "./useRef/UseRefDivCss";
import UseRefInputFocus from "./useRef/UseRefInputFocus";
import UseRefInputFocusCss from "./useRef/UseRefInputFocusCss";
import UseRefVsUseState from "./useRef/UseRefVsUseState";
import UseRefWithUseState from "./useRef/UseRefWithUseState";

function App() {
  return (
    <>
      {/*React Fragment*/}
      <UseRefVsUseState></UseRefVsUseState>
      <br />
      <UseRefInputFocus></UseRefInputFocus>
      <br />
      <UseRefInputFocusCss></UseRefInputFocusCss>
      <br />
      <UseRefDivCss></UseRefDivCss>
      <br />
      <UseRefWithUseState></UseRefWithUseState>
    </>
  );
}

export default App;
