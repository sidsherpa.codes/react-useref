import React, { useRef } from "react";

const UseRefInputFocusCss = () => {
  let inputRef1 = useRef();
  let inputRef2 = useRef();
  let inputRef3 = useRef();
  return (
    <div>
      <h3>useRef Input Focus & Css</h3>
      <form>
        <label htmlFor="name2">Name</label>
        <input
          id="name2"
          type="text"
          placeholder="Name"
          ref={inputRef1}
        ></input>
        <br />
        <input
          id="address"
          type="text"
          placeholder="Address"
          ref={inputRef2}
        ></input>
        <br />
        <input
          id="phoneNumber"
          type="text"
          placeholder="Phone Number"
          ref={inputRef3}
        ></input>
        <br />

        <button
          type="button"
          onClick={() => {
            inputRef1.current.focus();
            inputRef1.current.style.cssText =
              "background-color: #FF4D4D; color: white;"; //string literal for css
          }}
        >
          Click on Name
        </button>
        <button
          type="button"
          onClick={() => {
            inputRef2.current.focus();
            inputRef2.current.style.cssText =
              "background-color: #4DFF4D; color: #333;";
          }}
        >
          Click on Address
        </button>
        <button
          type="button"
          onClick={() => {
            inputRef3.current.focus();
            inputRef3.current.style.cssText =
              "background-color: #4D4DFF; color: white;";
          }}
        >
          Click on Phone number
        </button>
      </form>
    </div>
  );
};

export default UseRefInputFocusCss;
